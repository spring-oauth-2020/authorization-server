package com.muhardin.endy.belajar.oauth.authserver.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/home")
    public ModelMap homepage(Authentication currentUser){
        return new ModelMap()
                .addAttribute("currentUser", currentUser);
    }

}
